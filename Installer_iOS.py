'''A simple installer for the newApps suite, iOS only. Supports updates without losing data. (Files aren't transferred yet, just settings ie. Solitude Lock, themes)'''

import ui, os, zipfile, urllib.request, importlib.util, time

class Installer:
	@ui.in_background
	def install(sender):
		Installer.a.title = ' progress: 0/4'
		Installer.a.image = ui.Image.named('iob:ios7_clock_32')
		Installer.a.title = ' progress: 1/4 (this next part will take a moment)'
		if Installer.mirror.lower() == 'github':
			urllib.request.urlretrieve('https://codeload.github.com/Lachlantula/newApps/zip/%s' % Installer.version, 'NEWAPPS.zip')
		else:
			urllib.request.urlretrieve('https://gitlab.com/Lachlantula/newApps/repository/archive.zip?' + Installer.version, 'NEWAPPS.zip')
		Installer.a.title = ' progress: 2/4'
		if Installer.isInstalled == True: # if an install is already detected and is running the same version name-wise (ie. Unstable-pre1.0) nothing will be overwritten yet.
			Installer.v.close()
			print(os.listdir())
			while True:
				oldVersion = input('Please enter the version name of your old installer - e.g. "1.0": ')
				if os.path.isdir('newApps-' + oldVersion):
					break
				elif os.path.isdir(oldVersion):
					print('This installer isn\'t very smart right now (I just wanted to finish it ASAP), so please get rid of the newApps- prefix and enter just the version.')
				else:
					print('Doesn\'t exist')
			print('Porting settings...')
			os.rename('newApps-' + oldVersion, 'newApps-' + oldVersion + '-OLD')
			dir = importlib.util.spec_from_file_location('Old Shared', 'newApps-%s-OLD/settings/shared.py' % oldVersion)
			dir2 = importlib.util.spec_from_file_location('Old Solitude', 'newApps-%s-OLD/settings/solitude_cfg.py' % oldVersion)
			sharedModule = importlib.util.module_from_spec(dir)
			solitudeModule = importlib.util.module_from_spec(dir2)
			dir.loader.exec_module(sharedModule)
			dir2.loader.exec_module(solitudeModule)
			sharedSettings = sharedModule.sharedSettings
			solitudeSettings = solitudeModule.settings
		
		zip = zipfile.ZipFile('NEWAPPS.zip', 'r')
		zip.extractall()
		zip.close()
		
		Installer.a.title = ' progress: 3/4'
		
		if Installer.isInstalled == True:
			dir = importlib.util.spec_from_file_location('New Shared', 'newApps-%s/settings/shared.py' % Installer.version.replace('\n', ''))
			dir2 = importlib.util.spec_from_file_location('New Shared', 'newApps-%s/settings/solitude_cfg.py' % Installer.version.replace('\n', ''))
			sharedNew = importlib.util.module_from_spec(dir)
			solitudeNew = importlib.util.module_from_spec(dir2)
			dir.loader.exec_module(sharedNew)
			dir2.loader.exec_module(solitudeNew)
			sharedSettingsNew = sharedNew.sharedSettings
			solitudeSettingsNew = solitudeNew.settings
			sharedSettings = {**sharedSettingsNew, **sharedSettings}
			solitudeSettings = {**solitudeSettingsNew, **solitudeSettings}
			os.chdir('newApps-%s/settings' % Installer.version.replace('\n', ''))
			with open('shared.py', 'w+') as f:
				print(f.read())
				with open('sharedtext.txt', 'r') as f2:
					sharedtext = f2.read()
				f.write('sharedSettings = ' + str(sharedSettings) + '\n\n' + sharedtext)
			with open('solitude_cfg.py', 'w') as f:
				with open('overridetext.txt', 'r') as f2:
					overridetext = f2.read()
				f.write('settings = ' + str(solitudeSettings) + '\n\n' + overridetext)
			os.chdir('../..')
			print('Done merging settings, be sure to copy over your files!')
			time.sleep(2)
			Installer.v.present('sheet', title_bar_color = 'black')
		
		os.chdir('newApps-%s/solitude' % Installer.version.replace('\n', ''))
		os.rename('IMG_1389.PNG', 'fingerprint.png')
		os.rename('IMG_1390.PNG', 'lock_bg.png')
		os.rename('IMG_1391.PNG', 'lock_bg2.png')
		os.rename('IMG_1392.PNG', 'lock_bg3.png')
		os.rename('IMG_1393.PNG', 'logo.png')
		Installer.a.title = ' done'
		Installer.a.image = ui.Image.named('iob:checkmark_32')
		
	v = ui.View()
	a = ui.Button()
	isInstalled = False
	if any(x.startswith('newApps-') for x in os.listdir(os.getcwd())):
		isInstalled = True
		a.title = ' No settings/files will be lost.'
	a.image = ui.Image.named('iob:ios7_download_32')
	a.font = ('Futura', 10)
	a.x = 0
	a.y = 0
	a.tint_color = 'white'
	a.width = 300
	a.height = 250
	a.action = install
	b = ui.Label()
	while True:
		mirror = input('Choose download mirror: "GitHub" or "GitLab": ')
		if mirror.lower() == 'github' or mirror.lower() == 'gitlab':
			break
	if mirror.lower() == 'github':
		f = urllib.request.urlopen('https://raw.githubusercontent.com/Lachlantula/newApps/master/version.txt') # get version from github
	else:
		f = urllib.request.urlopen('https://gitlab.com/Lachlantula/newApps/raw/master/version.txt') # get version from gitlab
	version = f.read()
	version = version.decode('utf-8')
	# It's important to know it can sometimes take a few moments for GitHub to recognise the file (version.txt) update.
	b.text = 'Version: ' + version
	b.x = 0
	b.y = 250
	b.text_color = 'grey'
	b.alignment = ui.ALIGN_CENTER
	b.width = 300	
	b.height = 50
	v.add_subview(a)
	v.add_subview(b)
	v.background_color = 'black'
	v.width = 300	
	v.height = 300
	v.present('sheet', title_bar_color = 'black')

x = Installer()
