# newApps
## Gorgeous apps that are designed at both the programming and the UX level to be open and accessible.
### Screenshots
![Template in cyan](http://firres.site/template-cyan.png)
![Template in cyan, settings screen](http://firres.site/template-cyan-settings.png)
![Template in indigo, settings screen](http://firres.site/template-indigo-settings.png)
